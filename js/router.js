var root = null;
var useHash = false;
var router = new Navigo(root, useHash);
var DATA = {};

function routeTo(templateName, callback) {
    console.log("ROUTING!!!");
    $(document).ready(function(){
        $('#view').load('/templates/' + templateName + '.html', function() {
            console.log("ROUTING " + templateName.toUpperCase());
            navSelect(templateName);
            loadData(function() { callback(); });
        });
    });
}

router.on({
    'resume': function() { routeTo('resume', changeLanguage); },
    'contact': function() { routeTo('contact', changeLanguage); },
    'cl': function() { routeTo('cl', changeLanguage); },
    'references' :function() { routeTo('references', changeLanguage); },
    'portfolio' :function() { routeTo('portfolio', changeLanguage); },
    '*': function() { window.location = 'resume'; }
}).resolve();

router.notFound(function() { routeTo('404', changeLanguage); });

function navSelect(name) {
    $('.nav-item').each(function() {
        $(this).removeClass('selected');
    })

    if (name !== null) {
        $('#nav-'+name).addClass('selected');
    }
}

window.onload = function() {
    loadData(function() { changeLanguage(); });
}

function loadData(callback) {
    $(document).ready(function() {
        $.ajax({
            type: "GET",
            url: "data/data.csv",
            dataType: "text",
            success: function(data) {
                processData(data, callback);
            }
        })
    });
}

function processData(data, callback) {
    parsedData = Papa.parse(data)['data'];
    DATA = {};
    parsedData.forEach(el => {
        DATA[el[0]] = {"DE": el[1], "EN": el[2]};
    });
    callback();
}

function changeLanguage() {
    // get language
    lang = 'DE'
    langInput = document.querySelector('input[name="lang"]:checked')
    if (langInput !== null) lang = langInput.value;

    // set all fields
    for (var id in DATA) {
        var el = null;
        if (id !== '') el = document.getElementById(id);
        if (el !== null && !id.startsWith('warning')) {
            if (DATA[id][lang]) { el.innerHTML = DATA[id][lang]; }
        }
    }
}