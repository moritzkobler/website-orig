var timeout = null;

function activateModal(success) {
    var idName = success ? '#modal-success' : '#modal-failure'    
    var modalEl = $(idName).clone();
    modalEl.removeClass('invis');
    mui.overlay('on', modalEl[0]);
    timeout = setTimeout(deactivateModal, 5000);
}

function deactivateModal() {
    console.log("DEACTIVATING");
    clearTimeout(timeout);
    mui.overlay('off');
}