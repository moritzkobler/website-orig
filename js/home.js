var timeout = null;

$(document).ready(function() {
    $('body').on('mouseover', '#druid-link', function() {
        if (timeout !== null) { clearTimeout(timeout); }
        $('#druid-container').addClass('visible');
        timeout = setTimeout(function() {
            $('#druid-container').removeClass('visible');
            timeout = null;
        }, 3000);
    });
});