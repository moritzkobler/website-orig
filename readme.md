# Note
* In order to serve this locally, a local web server needs to be run.
* Ideally, this would be as easy as just running `py -m http.server 8080` from cmd.
* Unfortunately, we need to always serve the `index.html`, regardless of the route.
* This seems to be a bit harder, and likely requires something a bit more advanced that cannot just be run from cmd (apache, express, flask, ...).
* See https://github.com/krasimir/navigo/issues/230, also the `.htaccess` file is crucial for this purpose when actually publishing as it configures the server as needed.