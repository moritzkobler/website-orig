// contact form submit
$(document).ready(function() {
    $(document).on('click', '#con-form-submit', function(e) {
        var form = $('#con-message-form');
        // set language of validity check
        var lang = 'DE';
        var langInput = document.querySelector('input[name="lang"]:checked');
        if (langInput !== null) lang = langInput.value;
        
        // validate form and submit
        form.validate({ lang: lang.toLowerCase() });
        if (form.valid()) {
            submitContactForm();
            e.preventDefault();
        }
    });
});

function submitContactForm() {
    console.log("SUBMITTING THE FORM!");
    var formData = {
        'name': $('input[name=name]').val(),
        'email': $('input[name=email]').val(),
        'message': $('textarea[name=message]').val(),
        'captcha': grecaptcha.getResponse()
    }

    $.ajax({
        type: 'POST',
        url: '../php/form.php',
        data: formData,
        dataType: 'json',
        encode: true    
    }).done(function(data) {
        if (data['success'] == true) {
            console.log("SUCCESSFULLY SUBMITTED FORM");
            
            // reset captcha and form and activate success-modal (should be done via .each()...)
            grecaptcha.reset();
            document.getElementById('con-message-form').reset();
            $('#warning-con-form-name').removeClass('mui--is-dirty mui--is-not-empty mui--is-touched valid');
            $('#warning-con-form-name').addClass('mui--is-empty mui--is-untouched mui--is-pristine');
            $('#warning-con-form-email').removeClass('mui--is-dirty mui--is-not-empty mui--is-touched valid');
            $('#warning-con-form-email').addClass('mui--is-empty mui--is-untouched mui--is-pristine');
            $('#warning-con-form-message').removeClass('mui--is-dirty mui--is-not-empty mui--is-touched valid');
            $('#warning-con-form-message').addClass('mui--is-empty mui--is-untouched mui--is-pristine');
        }
        activateModal(data['success']);
    });
}