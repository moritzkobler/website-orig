<?php
    include 'ChromePhp.php';
    ChromePhp::log("Ich bin im PHP-Teil");

    $name; $email; $message; $captcha; $data; $errors;
    if (isset($_POST['name'])) { $name=$_POST['name']; } else { $errors['name'] = "No name given."; }
    if (isset($_POST['email'])) { $email=$_POST['email']; } else { $errors['email'] = "No email given."; }
    if (isset($_POST['message'])) { $message=$_POST['message']; } else { $errors['message'] = "No message given."; }
    $captcha=$_POST['g-recaptcha-response'];

    $secretKey = "6Lehcz8UAAAAABbwqyPMpV_4nXOa0K_AWXSZKz9O";
    $ip = $_SERVER['REMOTE_ADDR'];
    $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secretKey."&response=".$captcha."&remoteip=".$ip);
    $responseKeys = json_decode($response, true);
    
    if (intval($responseKeys["success"]) === 1) { $errors['captcha'] = "Captcha couldn't be validated"; }

    if (!empty($errors)) {
        $data['success'] = false;
        $data['errors'] = $errors;
    } else {
        mail("moritz.kobler@gmail.com", "Nachicht über moritzkobler.com von $name @ $email", $message);
        $data['success'] = true;
    }

    echo json_encode($data);